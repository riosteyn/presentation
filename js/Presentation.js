var Presentation = (function(_) {
    'use strict';

    var Presentation = function(presentationFiles, presentationName) {
        if (_.isEmpty(presentationFiles.slides) && _.isEmpty(presentationFiles.images)) {
            throw new Error('empty presentation');
        }

        this.presentationFromImages = false;
        this.files = this._processFiles(presentationFiles);
        this.name = presentationName;
        this.id = this._generateNewId();
    };

    Presentation.formats =  {
        slides: ['html'],
        images: ['jpg', 'png', 'gif'],
        styles: ['css']
    };

    Presentation._newPresentationId = 0;

    Presentation.prototype._generateNewId = function() {
        return Presentation._newPresentationId++;
    };

    Presentation.prototype._CLASS_SLIDE_IMAGE = 'slideImage';

    Presentation.prototype._createSlideHTMLFromImgUrl = function(url) {
        var $i = $('<img>', {
            src: url
        }).addClass(this._CLASS_SLIDE_IMAGE),
            $table = $('<table>'),
            $tr = $('<tr>'),
            $td = $('<td>');

        $td.append($i);
        $tr.append($td);
        $table.append($tr);

        return $table;
    };

    Presentation.prototype._processFiles = function(presentationFiles) {
        var slides, css;


        // слайды в картинках
        if (_.isEmpty(presentationFiles.slides)) {
            this.presentationFromImages = true;
            slides = _.map(presentationFiles.images, function(imgURL, fileName) {
                return {
                    fileName: fileName,
                    html: this._createSlideHTMLFromImgUrl(imgURL)
                };
            }, this);
            // слайды в html
        } else {
            slides = _.map(presentationFiles.slides, function(html, fileName) {
                return {
                    fileName: fileName,
                    html: this._setImagesInHtml(html, presentationFiles.images)
                };
            }, this);
        }

        slides.sort(function(a, b) {
            return a.fileName > b.fileName;
        });

        css = _.map(presentationFiles.styles, function(href, fileName) {
            return href;
        });

        return {
            slides: slides,
            css: css
        };
    };

    Presentation.prototype._setImagesInHtml = function(html, images) {
            var startIndex = 0, closeIndex = 0,
                str = '<img src="',
                imgName,
                result = '';

            while ( (startIndex = html.indexOf(str)) !== -1 ) {
                closeIndex = html.indexOf('"', startIndex + str.length + 1);
                result += html.slice(0, startIndex + str.length);
                imgName = html.slice(startIndex + str.length, closeIndex);
                result += images[imgName];
                html = html.slice(closeIndex);
            }

            result += html;
            return result;
    };

    return Presentation;
}(_));