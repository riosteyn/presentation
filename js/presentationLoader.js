/*
 PresentationLoader

     в конце загрузки в удобном формате возвращает файлы презентации в коллбек onEnd

    Загрузчик файлов. Методом .load читает файлы и генерирует события:
        1. Начало загрузки всех файлов
        2. Конец загрузки всех файлов

    Методы
        .isBusy boolean.

        .load
            Аргументы
                FilesArray

                cbs - object. Обработики на события начала/конца загрузки файла и на загрузку всех файлов
                    .onStart - без агрументов
                    .onEnd - аргумент обьект. Ключи как в knownFormats
                        .slides - array
                        .images - object
                        (key) fileName: (value) reader result
                        .styles - object
                        (key) fileName: (value) reader result

        .setFormats
            Агрументы
                knownFormats - object
                    все форматы str
                    .slides - массив всех читаемых форматов слайдов
                    .images - массив форматов картинок
                    .styles - массив определяемых форматов стилей
 */

var PresentationLoader = (function() {
    'use strict';

    var PresentationLoader = function(formats) {
        if (typeof formats !== 'object') {
            throw new TypeError('wrong argument ' + knownFormats);
        }

        if (!formats.slides.length || !formats.images.length || !formats.styles.length ) {
            throw new TypeError('wrong argument: all format types have to be arrays' + formats);
        }

        this._formats = formats;

        this._counter = null;
        this._busy = false;
        this.loadResults = null;

        this._EmptyLoadResults();
    };

    PresentationLoader.prototype.isBusy = function() {
        return this._busy;
    };

    PresentationLoader.prototype.load = function(files, cbs) {
        var fileName = null,
            i, format,
            reader,
            formatType;

        if (this._busy) {
            throw new Error('busy loader');
        }

        this.resetBeforeNewLoad();
        this._busy = true;

        this._counter = files.length;

        if (typeof cbs === 'object') {
            if (typeof cbs.onStart === 'function') {
                cbs.onStart();
            }
            if (typeof cbs.onEnd === 'function') {
                this.usr_onEnd = cbs.onEnd;
            }
        }

        for (i = 0; i < files.length; ++i) {
            fileName = files[i].name;
            format = this._getFormatFromFileName(fileName);
            formatType = this._getFormatType(format);

            if (formatType === null) {
                --this._counter;
                continue;
            }

            reader = new FileReader();
            reader.fileName = fileName;
            reader.format = format;
            reader.formatType = formatType;

            this._setListenersOnReader(reader, files[i]);
            this.loadResults[formatType][fileName] = null;
        }

        if (this._counter === 0) {
            this._onAllFilesLoaded();
        }
    };

    PresentationLoader.prototype._EmptyLoadResults = function() {
        var i,
            types = Object.keys(this._formats);

        this.loadResults = {};
        for (i = 0; i < types.length; ++i) {
            this.loadResults[types[i]] = {};
        }

        return true;
    };

    PresentationLoader.prototype._isFormatOfType = function(test, type) {
        if (!(type in this._formats)) {
            throw new Error('bad type ' + type + ' known types are' + Object.keys(this._formats));
        }
        return this._formats[type].indexOf(test) !== -1;
    };

    PresentationLoader.prototype._getFormatType = function(format) {
        var i,
            formatTypes = Object.keys(this._formats);

        for (i = 0; i < formatTypes.length; ++i) {
            if (this._isFormatOfType(format, formatTypes[i])) {
                return formatTypes[i];
            }
        }

        return null;
    };

    PresentationLoader.prototype.resetBeforeNewLoad = function() {
        this._counter = null;
        this._EmptyLoadResults();
        if (typeof this.usr_onEnd === 'function') {
            delete this.usr_onEnd;
        }
    };

    PresentationLoader.prototype._setListenersOnReader = function(reader, file) {
        reader.onloadend = function (e) {
            var target = e.target;

            this.loadResults[target.formatType][target.fileName] = e.target.result;

            --this._counter;

            if (this._counter === 0) {
              this._onAllFilesLoaded();
            }
        }.bind(this);

        if (reader.format === 'html') {
            reader.readAsText(file);
        } else {
            reader.readAsDataURL(file);
        }
    };

    PresentationLoader.prototype._onAllFilesLoaded = function() {
        this._busy = false;
        if (typeof this.usr_onEnd === 'function') {
            this.usr_onEnd(this.loadResults);
        }
        this.resetBeforeNewLoad();
    };

    PresentationLoader.prototype._getFormatFromFileName = function(fileName) {
        var split = fileName.split('.');
        return split[split.length - 1];
    };

    return PresentationLoader;
}());