var loader = new PresentationLoader(Presentation.formats);
var bd = new BDmini();
var viewsManager = new ViewsManager();

viewsManager.add('pList', new PresentationsList($('#viewPresentationsList')));
viewsManager.add('show', new PresentationShow($('#viewPresentationShow')));

viewsManager.show('pList');