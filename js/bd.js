var BDmini = (function (_) {
    'use strict';

    var Savior = function() {
        this.memo = {};
    };

    Savior.prototype.save = function(presentation) {
        if (typeof presentation !== 'object' || typeof presentation.id !== 'number') {
            throw new Error('bad presentation', presentation);
        }

        if (this.memo[presentation.id]) {
            throw new Error('update presentation');
        }

        this.memo[presentation.id] = presentation;
    };

    Savior.prototype.load = function(presentationId) {
        if (typeof presentationId !== 'number') {
            throw new Error('bad id');
        }

        if (!this.memo[presentationId]) {
            throw new Error('no presentation with id ' + presentationId);
        }

        return this.memo[presentationId];
    };

    Savior.prototype.getAllPresentations = function() {
        return _.values(this.memo);
    };

    return Savior;
}(_));