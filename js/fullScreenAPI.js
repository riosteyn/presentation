(function(w) {
    w._fullScreenAPI = (function() {
        var isFullScreen = (function() {
            if (document.fullscreenElement !== undefined) {
                return function() {
                    return !!document.fullscreenElement;
                }
            } else
            if (document.mozFullScreenElement !== undefined) {
                return function() {
                    return !!document.mozFullScreenElement;
                }
            } else {
                return function() {
                    return !!document.webkitFullscreenElement;
                }
            }
        }());

        var open = (function() {
            if (document.documentElement.requestFullscreen) {
                return document.documentElement.requestFullscreen;
            } else if (document.documentElement.mozRequestFullScreen) {
                return document.documentElement.mozRequestFullScreen;
            } else if (document.documentElement.webkitRequestFullscreen) {
                return function() {
                    document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                };
            }
        }()).bind(document.documentElement);

        var close = (function() {
            if (document.cancelFullScreen) {
                return document.cancelFullScreen;
            } else if (document.mozCancelFullScreen) {
                return document.mozCancelFullScreen;
            } else if (document.webkitCancelFullScreen) {
                return document.webkitCancelFullScreen;
            }
        }()).bind(document);

        var toggle = function(on) {
            var needOpen,
                isOn = isFullScreen();

            if (on === undefined) {
                needOpen = !isOn
            } else {
                if (on === isOn) {
                    return;
                }
                needOpen = on;
            }

            if (needOpen) {
                open();
            } else {
                close();
            }
        };

        return {
            isOpen: isFullScreen,
            toggle: toggle,
            open: open,
            close: close
        };
    }());
}(window));