/*
    Логика показа презентаций
 */

var PresentationShow = (function() {
    'use strict';

    var PresentationShow = function($main) {
        this.$el = $main;

        this.$els = {
            needFullScreenInput: $('#fullScreenOn'),
            needAnimationInput: $('#animationOn'),
            needProgressBar: $('#progressBarOn'),
            needStretchOn: $('#stretchOn'),
            presentation: $('#presentation'),
            progress: $('#progress'),
            progressBar: $('#progressBar'),
            hint: $('#hint'),
            slideSelection: $('#slideSelection'),
            stretchContainer: this.$el.find('.stretchControlContainer'),
            progressBarContainer: this.$el.find('.progressBarControlContainer'),
            animationContainer: this.$el.find('.animationControlContainer')
        };

        this._presentation = null;
        this._currentSlide = null;
        this._memoWindowWidth = null;

        this._mouseMoveDetection = {
            move: false,
            checkerTimeout: null,
            delay: 1500
        };

        this._onKeyDown = this._onKeyDown.bind(this);
        this._preventMouseScroll = this._preventMouseScroll.bind(this);
        this._onMouseMove = this._onMouseMove.bind(this);
        this._onResize = this._onResize.bind(this);

        this.$el.on('click', '#closeShowMode', function(e) {
            viewsManager.show('pList', bd.getAllPresentations());
        });

        this.$el.on('click', '#presentation', function() {
            this._changeSlide('RIGHT');
        }.bind(this));

        this.$el.on('change', '#slideSelection', function(e) {
            this._changeSlide('force', $(e.currentTarget).val());
        }.bind(this));

        this.$el.on('change', '#fullScreenOn', this._toggleFullScreen.bind(this));
        this.$el.on('change', '#progressBarOn', this._toggleProgress.bind(this));
        this.$el.on('change', '#stretchOn', this._toggleStretchFix.bind(this));
    };

    PresentationShow.prototype._CLASS_SLIDE_FROM_IMAGE = 'slideFromImage';
    PresentationShow.prototype._CLASS_SLIDE = 'slide';
    PresentationShow.prototype._CLASS_SHOW_HINT = 'hint';
    PresentationShow.prototype._CLASS_BODY_OVERFLOW_HIDDEN = 'no-overflow';
    PresentationShow.prototype._DEFAULT_ANIMATION_SPEED = 300;
    PresentationShow.prototype._KEY_CODES = {
        40: 'DOWN',
        38: 'UP',
        37: 'LEFT',
        39: 'RIGHT',
        27: 'ESC'
    };

    PresentationShow.prototype._toggleStretchFix = function(e) {
        if (this._presentation.presentationFromImages) {
            if (this._presentation.presentationFromImages) {
                this.$el.find('.' + this._CLASS_SLIDE).addClass(this._CLASS_SLIDE_FROM_IMAGE);
            }
            this.$els.stretchContainer.show();
            this._setStretch( this.$els.needStretchOn.is(':checked'));
        } else {
            this.$els.stretchContainer.hide();
        }
    };

    PresentationShow.prototype._toggleFullScreen = function() {
        if (this.$els.needFullScreenInput.is(':checked')) {
            window._fullScreenAPI.open();
        } else {
            window._fullScreenAPI.close();
        }
    };

    PresentationShow.prototype._toggleProgress = function() {
        var method = this.$els.needProgressBar.is(':checked')?'show': 'hide';

        this.$els.progressBar[method]();
        if (method === 'show') {
            this._setProgressBarTo(this._countProgressPercent(this._currentSlide));
        }
    };

    PresentationShow.prototype._onResize = function(e) {
        if (this._memoWindowWidth !== $(window).width()) {
            this.$els.presentation.empty();
            this._createPresentation();
            this._setScreenToSlide(this._currentSlide, true);
        }

        this.$els.needFullScreenInput.prop('checked', window._fullScreenAPI.isOpen());

        this._toggleStretchFix();
    };

    PresentationShow.prototype._onMouseMove = function(e) {
        clearTimeout(this._mouseMoveDetection.checkerTimeout);
        if (!this._mouseMoveDetection.move) {
            this.$el.addClass(this._CLASS_SHOW_HINT);
            this.$els.hint.fadeIn();
            this._mouseMoveDetection.move = true;
        }

        this._mouseMoveDetection.checkerTimeout = setTimeout(function() {
            this._onMouseStop({
                x: e.clientX,
                y: e.clientY
            });
        }.bind(this), this._mouseMoveDetection.delay);
    };

    PresentationShow.prototype._onMouseStop = function(stopCoords) {
        var rect = this.$els.hint[0].getBoundingClientRect();

        this._mouseMoveDetection.move = false;

        // мышь остановилась в #hint
        if (stopCoords.x >= rect.left && stopCoords.x <= rect.right && stopCoords.y >= rect.top && stopCoords.y <= rect.bottom ) {
            return;
        }

        this.$el.removeClass(this._CLASS_SHOW_HINT);
        this.$els.hint.fadeOut();
    };


    PresentationShow.prototype._preventMouseScroll = function(e) {
        e.preventDefault();
    };

    PresentationShow.prototype._countProgressPercent = function(slideNum) {
        return slideNum * 100 / (this._presentation.files.slides.length - 1);
    };

    PresentationShow.prototype._onKeyDown = function(e) {
        if ( !(e.keyCode in this._KEY_CODES)) {
            return;
        }

        var key = this._KEY_CODES[e.keyCode];

        // при фокусировке на инпуте можно выбирать слайд самим инпутом. Будет срабатывать change
        if (this.$els.slideSelection.is(':focus')) {
            if (key === 'DOWN' || key === 'UP') {
                return;
            }
        }

        e.preventDefault();
        if ( key === 'ESC') {
            viewsManager.show('pList', bd.getAllPresentations());
        } else {
            this._changeSlide(key);
        }
    };

    PresentationShow.prototype._changeSlide = function(key) {
        switch (key) {
            case 'force':
                this._currentSlide = +arguments[1];
                break;
            case 'LEFT':
                if (this._currentSlide === 0) {
                    return false;
                }
                this._currentSlide -= 1;
                break;
            case 'RIGHT':
                if (this._currentSlide === this._presentation.files.slides.length - 1) {
                    return false;
                }
                this._currentSlide += 1;
                break;
            case 'DOWN':
                this._currentSlide = 0;
                break;
            case 'UP':
                this._currentSlide = this._presentation.files.slides.length - 1;
                break;
        }

        this._setScreenToSlide(this._currentSlide);
        if (this.$els.needProgressBar.is(':checked')) {
            this._setProgressBarTo(this._countProgressPercent(this._currentSlide));
        }
        this.$els.slideSelection.val(this._currentSlide);
    };

    PresentationShow.prototype._setProgressBarTo = function(percent) {
        this.$els.progress.animate({
            width: percent + '%'
        }, this._getAnimationSpeed());
    };

    PresentationShow.prototype._setScreenToSlide = function(slideNum, forceNow) {
        var scrollValue = slideNum * $(window).width();

        this.$el.animate({
            marginLeft: -scrollValue
        }, (forceNow?0: this._getAnimationSpeed()));
    };

    PresentationShow.prototype._getAnimationSpeed = function() {
        return this.$els.needAnimationInput.is(':checked')? this._DEFAULT_ANIMATION_SPEED: 0;
    };

    PresentationShow.prototype._startShow = function() {
        this._currentSlide = 0;
        this._inputsToDefault();
        this.$els.slideSelection.val(this._currentSlide);
        this._createPresentation();
        this._generateSlideOptions(this._presentation.files.slides.length);

        this._toggleProgress();
        this._toggleStretchFix();
    };

    PresentationShow.prototype._inputsToDefault = function() {
        this.$els.needFullScreenInput.prop('checked', false);

        if (this._presentation.presentationFromImages) {
            this.$els.needStretchOn.prop('checked', false).show();
        } else {
            this.$els.needStretchOn.hide();
        }

        if (this._presentation.files.slides.length === 1) {
            this.$els.needProgressBar.prop('checked', false);
            this.$els.progressBarContainer.hide();

            this.$els.needAnimationInput.prop('checked', false);
            this.$els.animationContainer.hide();
        } else {
            this.$els.needProgressBar.prop('checked', true);
            this.$els.progressBarContainer.show();

            this.$els.needAnimationInput.prop('checked', true);
            this.$els.animationContainer.show();
        }
    };

    PresentationShow.prototype._generateSlideOptions = function(slidesNum) {
        var i, $o;

        for (i = 1; i <= slidesNum; ++i) {
            $o = $('<option>', {
                value: i - 1,
                html: 'слайд ' + i
            });
            this.$els.slideSelection.append($o);
        }
    };

    PresentationShow.prototype._createPresentation = function() {
        var i,
            $s;

        this._memoWindowWidth = $(window).width();

        this.$els.presentation.attr('data-id', this._presentation.id);

        for (i = 0; i < this._presentation.files.slides.length; ++i) {
            $s = $('<div>', {
                'class': this._CLASS_SLIDE,
                'id': this._CLASS_SLIDE + '_' + (i + 1)
            }).
                css({
                    width:  this._memoWindowWidth
                }).
                html(this._presentation.files.slides[i].html);
            this.$els.presentation.append($s);
        }

        for (i = 0; i < this._presentation.files.css.length; ++i) {
            this._addCss(this._presentation.files.css[i]);
        }

        this.$el.css({
            width: this._memoWindowWidth * this._presentation.files.slides.length
        });
    };

    PresentationShow.prototype._setStretch = function(isOn) {
        var $images = this.$el.find('.slideImage'),
            i,
            wWidth = $(window).width(),
            wHeight = $(window).height();

           if (isOn) {
               $images.css({
                   width: '100%',
                   height: '100%'
               });
           } else {
               for (i = 0; i < $images.length; ++i) {
                   this._stretchFix($images.eq(i), wWidth, wHeight);
               }
           }
    };

    PresentationShow.prototype._stretchFix = function($i, wWidth, wHeight) {
        var i = $i[0],
            nw = i.naturalWidth,
            nh = i.naturalHeight,
            ir = nw / nh,
            niw, nih, r = 1;

        if (ir > 1) {
            niw = wWidth;
            nih = wWidth / ir;

            if ( nih > wHeight) {
                r = nih / wHeight;
            }
        } else {
            nih = wHeight;
            niw = ir * wHeight;

            if (niw > wWidth) {
                r = niw / wWidth;
            }
        }

        $i.width(niw/r);
        $i.height(nih / r);
    };

    PresentationShow.prototype._addCss = function(href) {
        var $l = $('<link>', {
            rel: 'stylesheet',
            href: href
        }).attr('data-presentation-id', this._presentation.id);
        $('head').append($l);
    };

    PresentationShow.prototype.render = function(presentation) {
        this._presentation = presentation;
        var $body = $('body');

        $body.on('keydown', this._onKeyDown);
        $body.on('mousewheel wheel MozMousePixelScroll DOMMouseScroll wheel', this._preventMouseScroll);
        $body.on('mousemove', this._onMouseMove);
        $body.addClass(this._CLASS_BODY_OVERFLOW_HIDDEN);
        $(window).on('resize', this._onResize);

        this._startShow();

        return this;
    };

    PresentationShow.prototype.closeView = function() {
        this._currentSlide = 0;
        this._mouseMoveDetection.move = false;

        $(window).off('resize', this._onResize);
        window._fullScreenAPI.close();

        $('link[data-presentation-id]').remove();
        this.$els.presentation.empty();
        this.$els.slideSelection.empty();

        this.$el.removeClass(this._CLASS_SHOW_HINT).css({
            marginLeft: 0,
            top: 0
        });

        var $body = $('body');

        $body.off('keydown', this._onKeyDown);
        $body.off('mousewheel wheel MozMousePixelScroll DOMMouseScroll wheel', this._preventMouseScroll);
        $body.off('mousemove', this._onMouseMove);
        $body.removeClass(this._CLASS_BODY_OVERFLOW_HIDDEN);
    };

    return PresentationShow;
})();