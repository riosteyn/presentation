var ViewsManager = (function() {
    'use strict';

    var ViewsManager = function() {
        this._views = {};
        this._activeView = null;
        this._CLASS_ACTIVE_VIEW = 'active';
    };

    ViewsManager.prototype.add =function(viewName, view) {
        if ( viewName in this._views ) {
            throw new Error('view exists', viewName);
        }

        this._views[viewName] = view;
    };

    ViewsManager.prototype.show = function(viewName) {
        var previousView = null;

        if ( !(viewName in this._views) ) {
            throw new Error('unknown view', viewName);
        }

        if (this._activeView) {
            previousView = this._views[this._activeView];
            if (typeof previousView.closeView === 'function') {
                previousView.closeView();
            }
            previousView.$el.removeClass(this._CLASS_ACTIVE_VIEW);
        }

        var view = this._views[viewName],
            viewArguments = _.rest(arguments);

        view.render.apply(view, viewArguments);

        this._views[viewName].$el.addClass(this._CLASS_ACTIVE_VIEW);
        this._activeView = viewName;
    };

    return ViewsManager;
}());