var PresentationsList = (function() {
    'use strict';

    var Plist = function($main) {
        this.$el = $main;

        this.$els = {
            list: $('#pList'),
            listWrap: $('#listWrap'),
            inputName: $('#newPresentationName'),
            inputFiles: $('#newPresentationFiles'),
            noPresentations: $('#noPresentations'),
            addButton: $('button', this.$el),
            form: $('form', this.$el),
            stretchContainer: this.$el.find('.stretchControlContainer'),
            loader: this.$el.find('.loader')
        };

        this._memo_visited = {};

        this.$els.inputName.val(this._NEW_PRESENTATION_NAME_PLACEHOLDER_TEXT);

        this.$els.addButton.on('click', this._clickAddPresentation.bind(this));

        this.$el.on('click', '#pList .' + this._CLASS_PRESENTATION_NAME, this._clickOnPresentation.bind(this));

        this.$els.form.on('submit', function(e) {
            e.preventDefault();
        });

        this.$els.inputName.on('focus', function(e) {
            var $input = $(e.currentTarget);

            if ($input.val() === this._NEW_PRESENTATION_NAME_PLACEHOLDER_TEXT) {
                $(e.currentTarget).val('');
            }
        }.bind(this));

        this.$els.inputName.on('blur', function(e) {
            var $input = $(e.currentTarget);

            if (!$.trim($input.val())) {
                $input.val(this._NEW_PRESENTATION_NAME_PLACEHOLDER_TEXT);
            }
        }.bind(this));
    };

    Plist.prototype._NEW_PRESENTATION_NAME_PLACEHOLDER_TEXT = 'Новая презентация';
    Plist.prototype._CLASS_NEW_PRESENTATION = 'new';
    Plist.prototype._CLASS_PRESENTATION_NAME = 'pName';
    Plist.prototype._CLASS_NO_TEXT_OVERFLOW = 'noTextOverFlow';

    Plist.prototype._clickOnPresentation = function(e) {
        var $p = $(e.currentTarget),
            pId = +$p.attr('data-presentationId');

        this._memo_visited[pId] = true;

        viewsManager.show('show', bd.load(pId))
    };

    Plist.prototype._resetInputs = function() {
        this.$els.form[0].reset();
        this.$els.inputName.val(this._NEW_PRESENTATION_NAME_PLACEHOLDER_TEXT);
    };

    Plist.prototype._clickAddPresentation = function(e) {
        if (loader.isBusy()) {
            return;
        }

        e.preventDefault();
        var files = this.$els.inputFiles.prop('files'),
            presentationName = this.$els.inputName.val();

        if (files.length === 0) {
            alert('Добавьте файлы в презентацию.');
            return;
        }

        if (!presentationName) {
            alert('Введите название презентации.');
            return;
        }

        this.$els.loader.css('visibility', 'visible');

        loader.load(files, {
            onEnd: function(presentationFiles) {
                this.$els.loader.css('visibility', 'hidden');
                try {
                    var presentation = new Presentation(presentationFiles, presentationName);
                } catch(e) {
                    alert('Ошибка при добавлении презентации. Возможно в ней нет слайдов.');
                    return;
                }

                bd.save(presentation);
                this._addPresentationToList(presentation);
                this._resetInputs();
            }.bind(this)
        });
    };

    Plist.prototype._addPresentationToList = function(presentation) {
        if (!this.$els.list.is(':visible')) {
            this.$els.list.show();
            this.$els.noPresentations.hide();
        }

        var $li = this._getOnePresentation$Li(presentation);
        $li.hide();
        this.$els.list.append($li);
        $li.fadeIn();
    };

    Plist.prototype.render = function(presentations) {
        this.$els.list.empty();
        if (!presentations || presentations.length === 0) {
            this.$els.list.hide();
            this.$els.noPresentations.show();
        } else {
            var li = null,
                i;

            for (i = 0; i < presentations.length; ++i) {
                li = this._getOnePresentation$Li(presentations[i]);
                this.$els.list.append(li);
            }

            this.$els.listWrap.html(this.$els.list);
            this.$els.list.show();
            this.$els.noPresentations.hide();
        }

        return this;
    };

    Plist.prototype._getOnePresentation$Li = function(presentation) {
        var $li = $('<li>'),
            $d = $('<div>').addClass(this._CLASS_NO_TEXT_OVERFLOW),
            $span = $('<span>').attr('data-presentationId', presentation.id).addClass(this._CLASS_PRESENTATION_NAME).html(presentation.name);

        if ( !(presentation.id in this._memo_visited) ) {
            $span.addClass(this._CLASS_NEW_PRESENTATION);
        }

        $d.append($span);
        $li.append($d);

        return $li;
    };

    return Plist;
})();